package org.yourorghere;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import static org.yourorghere.Camara.canvas;


public class Cabeza  extends JFrame implements KeyListener {
    
    //Variables para funciones de OPENGL
    static GL gl;
    static GLU glu;
    static GLUT glut;
    static GLCanvas canvas;

    //Variables para la rotaci�n
    private static float rotarX = 0;
    private static float rotarY = 0;
    private static float rotarZ = 0;
    //Variables para la traslacion
    private static float trasladaX = 0;
    private static float trasladaY = 0;
    private static float trasladaZ = 0;
    
    
    
     public Cabeza() {
        //Propiedades para el frame
        setSize(600, 600);
        setLocationRelativeTo(null);
        setTitle("Manejo de la Camara en OPENGL");
        setResizable(false);
        //invocamos a la clase Graphic
        Cabeza.GraphicListener listener = new Cabeza.GraphicListener();
        //Se instancia al canvas y variables
        canvas = new GLCanvas();
        gl = canvas.getGL();
        glu = new GLU();
        glut = new GLUT();

        //Se a�anade el oyente que renderiza los graficos de el metodo
        //display
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);

        //Animaciones
        Animator animator = new Animator(canvas);
        animator.start();

        //A�adimos la interfaz keylistener
        addKeyListener(this);
    }

     
     public static void main(String[] args) {
        Cabeza ca=new Cabeza();
        ca.setVisible(true);
        ca.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
     
     
     
     
     
     
     
 public class GraphicListener implements GLEventListener {

        public void display(GLAutoDrawable arg0) {
            //inicializamos las variables OPENGl
            gl = arg0.getGL();
            //limpiamos el buffer
            gl.glClear(GL.GL_COLOR_BUFFER_BIT);
            //Establecemos el color de fondo de la ventana de colores RGB
            gl.glColor3f(0.5f, 0.5f, 0.5f);
            gl.glPushMatrix();
            gl.glMatrixMode(GL.GL_PROJECTION);
            gl.glLoadIdentity();

            //Establecer una perspectiva
            glu.gluPerspective(50, 2, 0, 30);
            //Variables de movimiento para la camara
            gl.glRotatef(rotarX, 1, 0, 0);
            gl.glRotatef(rotarY, 0, 1, 0);
            gl.glRotatef(rotarZ, 0, 0, 1);
            //Establecemos la camara
            glu.gluLookAt(0.5 + trasladaX, 0.5 + trasladaY, 10.5 + trasladaZ,0.5+trasladaX, 0,2.5+trasladaZ,0.0, 1.0, 0.0);
            //glu.gluLookAt(0, 0, 10, 0, 0, 3, 0, 1, 0);
            gl.glLineWidth(5);
            //Creamos ejes x,y,z
            //Eje X
          /*  gl.glBegin(gl.GL_LINES);
            gl.glColor3f(1, 0, 0);
            gl.glVertex3f(0, 0, 0);
            gl.glVertex3f(6, 0, 0);
            gl.glEnd();

            //Eje Y
            gl.glBegin(gl.GL_LINES);
            gl.glColor3f(0, 1, 0);
            gl.glVertex3f(0, 0, 0);
            gl.glVertex3f(0, 6, 0);
            gl.glEnd();

            //Eje z
            gl.glBegin(gl.GL_LINES);
            gl.glColor3f(0, 0, 1);
            gl.glVertex3f(0, 0, 0);
            gl.glVertex3f(0, 0, 6);
            gl.glEnd();*/

            //Invocar las figuras predefinidas de opengl    
            gl.glColor3f(255,0,255);
            glut.glutSolidSphere(3, 100, 100);
            
            gl.glTranslated(1, 0.5, 0.5);
            gl.glColor3f(255,100,190);
            glut.glutSolidSphere(.5, 16, 16);
            
            gl.glTranslated(1, 1.2, .5);
            glut.glutSolidSphere(.5,16,15);
            
            
            
            
         //   gl.glViewport(125, 125, 125, 125);
            
            
            
          
            gl.glFlush();

        }

        public void init(GLAutoDrawable drawable) {
        }

        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        }

        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }
    }

    //Se programan las teclas para manipular la camara
    public void keyPressed(KeyEvent arg0) {
       if(arg0.getKeyCode()==KeyEvent.VK_ESCAPE){
            rotarX=0;
            rotarY=0;
            rotarZ=0;
            trasladaX=0;
            trasladaY=0;
            trasladaZ=0;
        }
        if(arg0.getKeyCode()==KeyEvent.VK_X){
            rotarX+=1.0f;
            
        }

        if(arg0.getKeyCode()==KeyEvent.VK_A){
            rotarX-=1.0f;
            
        }

        if(arg0.getKeyCode()==KeyEvent.VK_Y){
            rotarY+=1.0f;
            
        }

        if(arg0.getKeyCode()==KeyEvent.VK_B){
            rotarY-=1.0f;
            
        }

        if(arg0.getKeyCode()==KeyEvent.VK_Z){
            rotarZ+=1.0f;
           
        }

        if(arg0.getKeyCode()==KeyEvent.VK_C){
            rotarZ-=1.0f;
           
        }

        //Camara
        if(arg0.getKeyCode()==KeyEvent.VK_RIGHT){
            trasladaX+=.10f;
            
        }

          if(arg0.getKeyCode()==KeyEvent.VK_LEFT){
            trasladaX-=.10f;
            
        }

         if(arg0.getKeyCode()==KeyEvent.VK_UP){
            trasladaY+=.10f;
            
        }

          if(arg0.getKeyCode()==KeyEvent.VK_DOWN){
            trasladaY-=.10f;
            
          }

        if(arg0.getKeyCode()==KeyEvent.VK_1){
            trasladaZ+=.1;
           
          }

        if(arg0.getKeyCode()==KeyEvent.VK_2){
            trasladaZ-=.10f;
           
          }
        if(arg0.getKeyCode()==KeyEvent.VK_3){
            rotarY+=1.0;
           
        }

        if(arg0.getKeyCode()==KeyEvent.VK_4){
            rotarY-=1.0;
           
        }
    }

    public void keyTyped(KeyEvent e) {
    }
    public void keyReleased(KeyEvent e) {
    }
}

package org.yourorghere;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;


public class Cara extends JFrame implements KeyListener{

    static GL gl;
    static GLU glu;
    static GLUT glut;
    static GLCanvas canvas;    
    private float rotar=0;
    
    public Cara() {
        //Propiedades para el frame
        setSize(600, 600);
        setLocationRelativeTo(null);
        setTitle("Cabeza");
        setResizable(false);
        //invocamos a la clase Graphic
        GraphicListener listener = new GraphicListener();
        //Se instancia al canvas y variables
        canvas = new GLCanvas();
        gl = canvas.getGL();
        glu = new GLU();
        glut = new GLUT();

        //Se a�anade el oyente que renderiza los graficos de el metodo
        //display
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);

        //Animaciones
        Animator animator = new Animator(canvas);
        animator.start();

        //A�adimos la interfaz keylistener
        addKeyListener(this);
    }
    
    
    
    public static void main(String[] args) {
        Cara ca= new Cara();
        ca.setVisible(true);
        ca.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    
    public class GraphicListener implements GLEventListener{

        @Override
        public void init(GLAutoDrawable drawable) {
             gl = drawable.getGL();
            gl.glEnable(gl.GL_BLEND);
            gl.glBlendFunc( gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA);  
        }

        @Override
        public void display(GLAutoDrawable drawable) {
            gl = drawable.getGL();
            //limpiamos el buffer
            gl.glClear(GL.GL_COLOR_BUFFER_BIT);
            //Establecemos el color de fondo de la ventana de colores RGB
            gl.glClearColor(1f, .9f, .9f, 1f);
            //gl.glColor3f(0.5f, 0.5f, 0.5f);
            gl.glMatrixMode(GL.GL_PROJECTION);
            gl.glLoadIdentity();
            
            glu.gluPerspective(50, 2, 0, 30);
            //Establecemos la camara
            glu.gluLookAt(0, 0, 10, 0, 0, 3, 0, 1, 0);
            gl.glLineWidth(5);
            
            
//          <<<<<<<<<<<<<<<<====================================
            
            
            gl.glRotatef(rotar,0,1,0);
            gl.glColor3f(128,0,0);
            glut.glutSolidSphere(3, 100, 100);
            gl.glFlush();
            gl.glEnd();
            
            /*gl.glTranslatef(-1, 1.5f, 1);
            gl.glColor3f(255,255,255);
            glut.glutSolidSphere(.5, 20, 20);
            gl.glFlush();
            gl.glEnd();
            
            gl.glTranslatef(0, 0 , 0);
            gl.glColor3f(0,0,0);
            glut.glutSolidSphere(.2,20,20);
            gl.glFlush();
            gl.glEnd();*/
            
            //gl.glTranslatef(0,0,0);            
            //glut.glutSolidCone(.5f, 1, 20, 20);
            
            
     /*       gl.glColor3f(30,30,30);
            Nariz(.5,1,0f,0f,-.2f);
            
         
            
            gl.glColor3f(255,255,255);
            Circulos(.5 ,-1f ,1.5f, 0);
            Circulos(.45, 1.85f, -.1f, 0);
            
            
            gl.glColor3f(0,0,0);
            Circulos(.1,0,0,0);
            Circulos(.1,-1.7f,0f,0);
            gl.glFlush();
            gl.glEnd();
            
            gl.glColor3f(0,255,255);
            Circulos(.8,.7f,-3f,0);
            */
            
            
            
        }

        @Override
        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

        }

        @Override
        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
            
        }
        
    }
    
        public void Nariz(double t,double t1, float px, float py, float pz){
            gl.glTranslatef(px,py,pz);
            glut.glutSolidCone(t, t1, 20, 20);
            gl.glFlush();
            gl.glEnd();
            
        }
       public void Circulos(double t, float px, float py, float pz){          
           
           gl.glTranslatef(px,py,pz);
           glut.glutSolidSphere(t, 20, 20);
           gl.glFlush();
           gl.glEnd();
           
       }
    
    @Override
    public void keyTyped(KeyEvent ke) {
        
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        
        if(ke.getKeyCode()==KeyEvent.VK_A )
            rotar++;
        
        if(ke.getKeyCode()==KeyEvent.VK_S)
            rotar--;
        
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        
    }
    
    
    
}
